﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CovidTracing.Resources;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CovidTracing.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public ICollection<Organisation> Organisations { get; set; } = new List<Organisation>();
        public ICollection<SubUser> SubUsers { get; set; } = new List<SubUser>();
    }

    public class Organisation
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public ICollection<Venue> Venues { get; set; } = new List<Venue>();
        public ICollection<ApplicationUser> Users { get; set; } = new List<ApplicationUser>();
        public ICollection<VisitationReason> VisitationReasons { get; set; } = new List<VisitationReason>();
    }

    public class VisitationReason
    {
        public long Id { get; set; }
        public long OrganisationId { get; set; }
        public string Description { get; set; }
        public Organisation Organisation { get; set; }
    }

    public class SubUser
    {
        public long Id { get; set; }
        public string MainUserId { get; set; }
        public string UserCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public ApplicationUser MainUser { get; set; }
        public ICollection<DailyVisitorRegistration> DailyVisitorRegistrations { get; set; } = new List<DailyVisitorRegistration>();
    }

    public class Venue
    {
        public long Id { get; set; }
        public long OrganisationId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public Organisation Organisation { get; set; }

        public ICollection<VenueRegistration> Registrations { get; set; } = new List<VenueRegistration>();
    }

    public class VenueRegistration
    {
        public long Id { get; set; }
        public string LoggedById { get; set; }
        public long VenueId { get; set; }
        public long DailyUserRegistrationId { get; set; }
        public DateTime Created { get; set; }
        public float? Temperature { get; set; }
        public Venue Venue { get; set; }
        public ApplicationUser LoggedBy { get; set; }
        public DailyVisitorRegistration DailyVisitorRegistration { get; set; }
    }

    public class RegistrationQuestionVersion
    {
        public long Id { get; set; }
        public string QuestionGroup1 { get; set; }
        public string Question1 { get; set; }
        public string Question2 { get; set; }
        public string Question3 { get; set; }
        public string Question4 { get; set; }
        public string Question5 { get; set; }
        public string Question6 { get; set; }
        public string Question7 { get; set; }
        public string Question8 { get; set; }
        public string Question9 { get; set; }
        public string Question10 { get; set; }
        public string Question11 { get; set; }
        public string Question12 { get; set; }
        public string Question13 { get; set; }
        public string Question14 { get; set; }
        public string Question15 { get; set; }
        public string Question16 { get; set; }
        public string Question17 { get; set; }
        public string Question18 { get; set; }
        public string Question19 { get; set; }
        public string Question20 { get; set; }
        public DateTime Created { get; set; }

        public ICollection<DailyVisitorRegistration> DailyUserRegistrations { get; set; } =
            new List<DailyVisitorRegistration>();
    }

    public class DailyVisitorRegistration
    {
        public long Id { get; set; }
        public long RegistrationQuestionVersionId { get; set; }
        public long SubUserId { get; set; }

        public string Answer1 { get; set; }
        public string Answer2 { get; set; }
        public string Answer3 { get; set; }
        public string Answer4 { get; set; }
        public string Answer5 { get; set; }
        public string Answer6 { get; set; }
        public string Answer7 { get; set; }
        public string Answer8 { get; set; }
        public string Answer9 { get; set; }
        public string Answer10 { get; set; }
        public string Answer11 { get; set; }
        public string Answer12 { get; set; }
        public string Answer13 { get; set; }
        public string Answer14 { get; set; }
        public string Answer15 { get; set; }
        public string Answer16 { get; set; }
        public string Answer17 { get; set; }
        public string Answer18 { get; set; }
        public string Answer19 { get; set; }
        public string Answer20 { get; set; }

        public RegistrationQuestionVersion RegistrationQuestionVersion { get; set; }
        public DateTime Created { get; set; }
        public SubUser SubUser { get; set; }
        public string AddressLine1 { get; internal set; }
        public string AddressLine2 { get; internal set; }
        public string City { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PostalCode { get; set; }
        public string Suburb { get; set; }
        public DateTime Updated { get; set; }
        public ICollection<VenueRegistration> VenueRegistrations { get; set; } = new List<VenueRegistration>();
    }

    public class UserCode
    {
        [Key]
        public string Value { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", false)
        {
            CurrentRegistrationQuestionVersion =
                RegistrationQuestionVersions.OrderByDescending(o => o.Created).FirstOrDefault();
        }

        private string GetTestUserCode()
        {
            var chars1 = "ABCDEFGHJKMNOPQRSTUVWYZ";
            var chars2 = "123456789";
            var chars3 = "-#:.,";

            var stringChars1 = new char[3];
            var stringChars2 = new char[4];
            var random = new Random();

            for (int i = 0; i < stringChars1.Length; i++)
            {
                stringChars1[i] = chars1[random.Next(chars1.Length)];
            }

            for (int i = 0; i < stringChars2.Length; i++)
            {
                stringChars2[i] = chars2[random.Next(chars2.Length)];
            }

            return $"{new String(stringChars1)}{chars3[random.Next(chars3.Length)]}{new String(stringChars2)}";
        }

        private static Object UserCodePadlock = new object();
        public string GetUserCode()
        {
            var startValue = GetTestUserCode();
            while (UserCodes.Any(o => o.Value == startValue))
            {
                startValue = GetTestUserCode();
            }

            string result = null;
            //lock and try again (in case it happened before locking)
            lock (UserCodePadlock)
            {

                if (!UserCodes.Any(o => o.Value == startValue))
                {
                    result = startValue;
                    UserCodes.Add(new UserCode { Value = startValue});
                    SaveChanges();
                }
            }

            return result ?? GetUserCode();
        }

        public DbSet<UserCode> UserCodes { get; set; }
        public DbSet<DailyVisitorRegistration> DailyVisitorRegistrations { get; set; }
        public DbSet<VenueRegistration> VenueRegistrations { get; set; }
        public DbSet<Venue> Venues { get; set; }
        public DbSet<Organisation> Organisations { get; set; }
        public DbSet<RegistrationQuestionVersion> RegistrationQuestionVersions { get; set; }
        public RegistrationQuestionVersion CurrentRegistrationQuestionVersion { get; private set; }
        public DbSet<SubUser> SubUsers { get; set; }


        public void CheckRegistrationQuestionVersions()
        {
            var questionHash =
                $"{Questions.Question1}{Questions.Question2}{Questions.Question3}{Questions.Question4}{Questions.Question5}{Questions.Question6}{Questions.Question7}{Questions.Question8}{Questions.Question9}{Questions.Question10}{Questions.Question11}{Questions.Question12}{Questions.Question13}{Questions.Question14}{Questions.Question15}{Questions.Question16}{Questions.Question17}{Questions.Question18}{Questions.Question19}{Questions.Question20}{Questions.QuestionGroup1}"
                    .GetHashCode();
            var qv = RegistrationQuestionVersions.OrderByDescending(o => o.Created).FirstOrDefault();
            var currentHash =
                $"{qv?.Question1}{qv?.Question2}{qv?.Question3}{qv?.Question4}{qv?.Question5}{qv?.Question6}{qv?.Question7}{qv?.Question8}{qv?.Question9}{qv?.Question10}{qv?.Question11}{qv?.Question12}{qv?.Question13}{qv?.Question14}{qv?.Question15}{qv?.Question16}{qv?.Question17}{qv?.Question18}{qv?.Question19}{qv?.Question20}{qv?.QuestionGroup1}"
                    .GetHashCode();
            if (questionHash != currentHash)
            {
                qv = new RegistrationQuestionVersion
                {
                    Created = DateTime.Now,
                    QuestionGroup1 = Questions.QuestionGroup1,
                    Question1 = Questions.Question1,
                    Question2 = Questions.Question2,
                    Question3 = Questions.Question3,
                    Question4 = Questions.Question4,
                    Question5 = Questions.Question5,
                    Question6 = Questions.Question6,
                    Question7 = Questions.Question7,
                    Question8 = Questions.Question8,
                    Question9 = Questions.Question9,
                    Question10 = Questions.Question10,
                    Question11 = Questions.Question11,
                    Question12 = Questions.Question12,
                    Question13 = Questions.Question13,
                    Question14 = Questions.Question14,
                    Question15 = Questions.Question15,
                    Question16 = Questions.Question16,
                    Question17 = Questions.Question17,
                    Question18 = Questions.Question18,
                    Question19 = Questions.Question19,
                    Question20 = Questions.Question20
                };
                RegistrationQuestionVersions.Add(qv);
                SaveChanges();
            }

            CurrentRegistrationQuestionVersion = qv;
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}