﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using CovidTracing.Models;
using QRCoder;

namespace CovidTracing.Infrastructure
{
    public static class QrCodeHelper
    {
        public static byte[] GetQrCode(SubUser data, string framePath, string imagePath = null)
        {
            var qrGenerator = new QRCodeGenerator();
            var qrCodeData = qrGenerator.CreateQrCode(data.Id.ToString(), QRCodeGenerator.ECCLevel.Q);
            var qrCode = new QRCode(qrCodeData);

            Bitmap qrCodeBmp;
            if (!string.IsNullOrEmpty(imagePath))
            {
                //Set color by using Color-class types
                qrCodeBmp = qrCode.GetGraphic(20, Color.DarkRed, Color.PaleGreen, (Bitmap)Bitmap.FromFile(imagePath));
            }
            else
            {
                //Set color by using Color-class types
                qrCodeBmp = qrCode.GetGraphic(20, Color.DarkRed, Color.PaleGreen, true);
            }

            qrCodeBmp.MakeTransparent(Color.PaleGreen);

            var frameBmp = new Bitmap(framePath);
            RectangleF rectf = new RectangleF(100, 35, 500, 50);
            Graphics g = Graphics.FromImage(frameBmp);

            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            g.DrawString(data.UserCode, new Font("Tahoma", 12), Brushes.Black, rectf);
            g.DrawImage(qrCodeBmp, new Point(50,50));
            g.Flush();

            using (MemoryStream ms = new MemoryStream())
            {
                //qrCodeBmp.Save(ms, ImageFormat.Bmp);
                frameBmp.Save(ms, ImageFormat.Bmp);
                return ms.ToArray();
            }
        }
    }
}