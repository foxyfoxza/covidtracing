namespace CovidTracing.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RegistrationQuestionVersions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RegistrationQuestionVersions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        QuestionGroup1 = c.String(),
                        Question1 = c.String(),
                        Question2 = c.String(),
                        Question3 = c.String(),
                        Question4 = c.String(),
                        Question5 = c.String(),
                        Question6 = c.String(),
                        Question7 = c.String(),
                        Question8 = c.String(),
                        Question9 = c.String(),
                        Question10 = c.String(),
                        Question11 = c.String(),
                        Question12 = c.String(),
                        Question13 = c.String(),
                        Question14 = c.String(),
                        Question15 = c.String(),
                        Question16 = c.String(),
                        Question17 = c.String(),
                        Question18 = c.String(),
                        Question19 = c.String(),
                        Question20 = c.String(),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.DailyUserRegistrations", "RegistrationQuestionVersionId", c => c.Long(nullable: false));
            CreateIndex("dbo.DailyUserRegistrations", "RegistrationQuestionVersionId");
            AddForeignKey("dbo.DailyUserRegistrations", "RegistrationQuestionVersionId", "dbo.RegistrationQuestionVersions", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DailyUserRegistrations", "RegistrationQuestionVersionId", "dbo.RegistrationQuestionVersions");
            DropIndex("dbo.DailyUserRegistrations", new[] { "RegistrationQuestionVersionId" });
            DropColumn("dbo.DailyUserRegistrations", "RegistrationQuestionVersionId");
            DropTable("dbo.RegistrationQuestionVersions");
        }
    }
}
