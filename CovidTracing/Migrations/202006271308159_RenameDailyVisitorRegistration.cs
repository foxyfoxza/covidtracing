namespace CovidTracing.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameDailyVisitorRegistration : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.DailyUserRegistrations", newName: "DailyVisitorRegistrations");
            DropForeignKey("dbo.VenueRegistrations", "DailyUserRegistrationId", "dbo.DailyUserRegistrations");
            DropIndex("dbo.VenueRegistrations", new[] { "DailyUserRegistrationId" });
            AddColumn("dbo.VenueRegistrations", "DailyVisitorRegistration_Id", c => c.Long());
            CreateIndex("dbo.VenueRegistrations", "DailyVisitorRegistration_Id");
            AddForeignKey("dbo.VenueRegistrations", "DailyVisitorRegistration_Id", "dbo.DailyVisitorRegistrations", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VenueRegistrations", "DailyVisitorRegistration_Id", "dbo.DailyVisitorRegistrations");
            DropIndex("dbo.VenueRegistrations", new[] { "DailyVisitorRegistration_Id" });
            DropColumn("dbo.VenueRegistrations", "DailyVisitorRegistration_Id");
            CreateIndex("dbo.VenueRegistrations", "DailyUserRegistrationId");
            AddForeignKey("dbo.VenueRegistrations", "DailyUserRegistrationId", "dbo.DailyUserRegistrations", "Id", cascadeDelete: true);
            RenameTable(name: "dbo.DailyVisitorRegistrations", newName: "DailyUserRegistrations");
        }
    }
}
