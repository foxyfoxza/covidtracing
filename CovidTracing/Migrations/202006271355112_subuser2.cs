namespace CovidTracing.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class subuser2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DailyVisitorRegistrations",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        RegistrationQuestionVersionId = c.Long(nullable: false),
                        SubUserId = c.Long(nullable: false),
                        Answer1 = c.String(),
                        Answer2 = c.String(),
                        Answer3 = c.String(),
                        Answer4 = c.String(),
                        Answer5 = c.String(),
                        Answer6 = c.String(),
                        Answer7 = c.String(),
                        Answer8 = c.String(),
                        Answer9 = c.String(),
                        Answer10 = c.String(),
                        Answer11 = c.String(),
                        Answer12 = c.String(),
                        Answer13 = c.String(),
                        Answer14 = c.String(),
                        Answer15 = c.String(),
                        Answer16 = c.String(),
                        Answer17 = c.String(),
                        Answer18 = c.String(),
                        Answer19 = c.String(),
                        Answer20 = c.String(),
                        Created = c.DateTime(nullable: false),
                        AddressLine1 = c.String(),
                        AddressLine2 = c.String(),
                        City = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        PostalCode = c.String(),
                        Suburb = c.String(),
                        Updated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RegistrationQuestionVersions", t => t.RegistrationQuestionVersionId, cascadeDelete: true)
                .ForeignKey("dbo.SubUsers", t => t.SubUserId, cascadeDelete: true)
                .Index(t => t.RegistrationQuestionVersionId)
                .Index(t => t.SubUserId);
            
            AddColumn("dbo.VenueRegistrations", "DailyVisitorRegistration_Id", c => c.Long());
            CreateIndex("dbo.VenueRegistrations", "DailyVisitorRegistration_Id");
            AddForeignKey("dbo.VenueRegistrations", "DailyVisitorRegistration_Id", "dbo.DailyVisitorRegistrations", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VenueRegistrations", "DailyVisitorRegistration_Id", "dbo.DailyVisitorRegistrations");
            DropForeignKey("dbo.DailyVisitorRegistrations", "SubUserId", "dbo.SubUsers");
            DropForeignKey("dbo.DailyVisitorRegistrations", "RegistrationQuestionVersionId", "dbo.RegistrationQuestionVersions");
            DropIndex("dbo.VenueRegistrations", new[] { "DailyVisitorRegistration_Id" });
            DropIndex("dbo.DailyVisitorRegistrations", new[] { "SubUserId" });
            DropIndex("dbo.DailyVisitorRegistrations", new[] { "RegistrationQuestionVersionId" });
            DropColumn("dbo.VenueRegistrations", "DailyVisitorRegistration_Id");
            DropTable("dbo.DailyVisitorRegistrations");
        }
    }
}
