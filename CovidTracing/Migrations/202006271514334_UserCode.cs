namespace CovidTracing.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserCode : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserCodes",
                c => new
                    {
                        Value = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Value);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserCodes");
        }
    }
}
