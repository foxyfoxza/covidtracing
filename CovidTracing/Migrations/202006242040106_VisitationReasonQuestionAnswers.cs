namespace CovidTracing.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VisitationReasonQuestionAnswers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.VisitationReasons",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        OrganisationId = c.Long(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Organisations", t => t.OrganisationId, cascadeDelete: true)
                .Index(t => t.OrganisationId);
            
            AddColumn("dbo.DailyUserRegistrations", "Answer1", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "Answer2", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "Answer3", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "Answer4", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "Answer5", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "Answer6", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "Answer7", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "Answer8", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "Answer9", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "Answer10", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "Answer11", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "Answer12", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "Answer13", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "Answer14", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "Answer15", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "Answer16", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "Answer17", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "Answer18", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "Answer19", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "Answer20", c => c.String());
            DropColumn("dbo.DailyUserRegistrations", "Q1");
            DropColumn("dbo.DailyUserRegistrations", "Q2");
            DropColumn("dbo.DailyUserRegistrations", "Q3");
            DropColumn("dbo.DailyUserRegistrations", "Q4");
            DropColumn("dbo.DailyUserRegistrations", "Q5");
            DropColumn("dbo.DailyUserRegistrations", "Q6");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DailyUserRegistrations", "Q6", c => c.Boolean(nullable: false));
            AddColumn("dbo.DailyUserRegistrations", "Q5", c => c.Boolean(nullable: false));
            AddColumn("dbo.DailyUserRegistrations", "Q4", c => c.Boolean(nullable: false));
            AddColumn("dbo.DailyUserRegistrations", "Q3", c => c.Boolean(nullable: false));
            AddColumn("dbo.DailyUserRegistrations", "Q2", c => c.Boolean(nullable: false));
            AddColumn("dbo.DailyUserRegistrations", "Q1", c => c.Boolean(nullable: false));
            DropForeignKey("dbo.VisitationReasons", "OrganisationId", "dbo.Organisations");
            DropIndex("dbo.VisitationReasons", new[] { "OrganisationId" });
            DropColumn("dbo.DailyUserRegistrations", "Answer20");
            DropColumn("dbo.DailyUserRegistrations", "Answer19");
            DropColumn("dbo.DailyUserRegistrations", "Answer18");
            DropColumn("dbo.DailyUserRegistrations", "Answer17");
            DropColumn("dbo.DailyUserRegistrations", "Answer16");
            DropColumn("dbo.DailyUserRegistrations", "Answer15");
            DropColumn("dbo.DailyUserRegistrations", "Answer14");
            DropColumn("dbo.DailyUserRegistrations", "Answer13");
            DropColumn("dbo.DailyUserRegistrations", "Answer12");
            DropColumn("dbo.DailyUserRegistrations", "Answer11");
            DropColumn("dbo.DailyUserRegistrations", "Answer10");
            DropColumn("dbo.DailyUserRegistrations", "Answer9");
            DropColumn("dbo.DailyUserRegistrations", "Answer8");
            DropColumn("dbo.DailyUserRegistrations", "Answer7");
            DropColumn("dbo.DailyUserRegistrations", "Answer6");
            DropColumn("dbo.DailyUserRegistrations", "Answer5");
            DropColumn("dbo.DailyUserRegistrations", "Answer4");
            DropColumn("dbo.DailyUserRegistrations", "Answer3");
            DropColumn("dbo.DailyUserRegistrations", "Answer2");
            DropColumn("dbo.DailyUserRegistrations", "Answer1");
            DropTable("dbo.VisitationReasons");
        }
    }
}
