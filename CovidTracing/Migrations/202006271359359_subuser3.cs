namespace CovidTracing.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class subuser3 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ApplicationUserOrganisations", newName: "OrganisationApplicationUsers");
            DropPrimaryKey("dbo.OrganisationApplicationUsers");
            AddPrimaryKey("dbo.OrganisationApplicationUsers", new[] { "Organisation_Id", "ApplicationUser_Id" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.OrganisationApplicationUsers");
            AddPrimaryKey("dbo.OrganisationApplicationUsers", new[] { "ApplicationUser_Id", "Organisation_Id" });
            RenameTable(name: "dbo.OrganisationApplicationUsers", newName: "ApplicationUserOrganisations");
        }
    }
}
