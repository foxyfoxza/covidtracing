namespace CovidTracing.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VenueAndRegister : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DailyUserRegistrations",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        Q1 = c.Boolean(nullable: false),
                        Q2 = c.Boolean(nullable: false),
                        Q3 = c.Boolean(nullable: false),
                        Q4 = c.Boolean(nullable: false),
                        Q5 = c.Boolean(nullable: false),
                        Q6 = c.Boolean(nullable: false),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Organisations",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Code = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Venues",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        OrganisationId = c.Long(nullable: false),
                        Name = c.String(),
                        Code = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Organisations", t => t.OrganisationId, cascadeDelete: true)
                .Index(t => t.OrganisationId);
            
            CreateTable(
                "dbo.VenueRegistrations",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        LoggedById = c.String(maxLength: 128),
                        VenueId = c.Long(nullable: false),
                        DailyUserRegistrationId = c.Long(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Temperature = c.Single(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DailyUserRegistrations", t => t.DailyUserRegistrationId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.LoggedById)
                .ForeignKey("dbo.Venues", t => t.VenueId, cascadeDelete: true)
                .Index(t => t.LoggedById)
                .Index(t => t.VenueId)
                .Index(t => t.DailyUserRegistrationId);
            
            CreateTable(
                "dbo.OrganisationApplicationUsers",
                c => new
                    {
                        Organisation_Id = c.Long(nullable: false),
                        ApplicationUser_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.Organisation_Id, t.ApplicationUser_Id })
                .ForeignKey("dbo.Organisations", t => t.Organisation_Id, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id, cascadeDelete: true)
                .Index(t => t.Organisation_Id)
                .Index(t => t.ApplicationUser_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DailyUserRegistrations", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.VenueRegistrations", "VenueId", "dbo.Venues");
            DropForeignKey("dbo.VenueRegistrations", "LoggedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.VenueRegistrations", "DailyUserRegistrationId", "dbo.DailyUserRegistrations");
            DropForeignKey("dbo.Venues", "OrganisationId", "dbo.Organisations");
            DropForeignKey("dbo.OrganisationApplicationUsers", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrganisationApplicationUsers", "Organisation_Id", "dbo.Organisations");
            DropIndex("dbo.OrganisationApplicationUsers", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.OrganisationApplicationUsers", new[] { "Organisation_Id" });
            DropIndex("dbo.VenueRegistrations", new[] { "DailyUserRegistrationId" });
            DropIndex("dbo.VenueRegistrations", new[] { "VenueId" });
            DropIndex("dbo.VenueRegistrations", new[] { "LoggedById" });
            DropIndex("dbo.Venues", new[] { "OrganisationId" });
            DropIndex("dbo.DailyUserRegistrations", new[] { "UserId" });
            DropTable("dbo.OrganisationApplicationUsers");
            DropTable("dbo.VenueRegistrations");
            DropTable("dbo.Venues");
            DropTable("dbo.Organisations");
            DropTable("dbo.DailyUserRegistrations");
        }
    }
}
