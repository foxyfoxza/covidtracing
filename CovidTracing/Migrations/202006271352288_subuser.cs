namespace CovidTracing.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class subuser : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.OrganisationApplicationUsers", newName: "ApplicationUserOrganisations");
            DropForeignKey("dbo.DailyVisitorRegistrations", "RegistrationQuestionVersionId", "dbo.RegistrationQuestionVersions");
            DropForeignKey("dbo.VenueRegistrations", "DailyVisitorRegistration_Id", "dbo.DailyVisitorRegistrations");
            DropForeignKey("dbo.DailyVisitorRegistrations", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.DailyVisitorRegistrations", new[] { "RegistrationQuestionVersionId" });
            DropIndex("dbo.DailyVisitorRegistrations", new[] { "UserId" });
            DropIndex("dbo.VenueRegistrations", new[] { "DailyVisitorRegistration_Id" });
            DropPrimaryKey("dbo.ApplicationUserOrganisations");
            CreateTable(
                "dbo.SubUsers",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        MainUserId = c.String(maxLength: 128),
                        UserCode = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        AddressLine1 = c.String(),
                        AddressLine2 = c.String(),
                        Suburb = c.String(),
                        City = c.String(),
                        PostalCode = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.MainUserId)
                .Index(t => t.MainUserId);
            
            AddPrimaryKey("dbo.ApplicationUserOrganisations", new[] { "ApplicationUser_Id", "Organisation_Id" });
            DropColumn("dbo.AspNetUsers", "FirstName");
            DropColumn("dbo.AspNetUsers", "LastName");
            DropColumn("dbo.AspNetUsers", "AddressLine1");
            DropColumn("dbo.AspNetUsers", "AddressLine2");
            DropColumn("dbo.AspNetUsers", "Suburb");
            DropColumn("dbo.AspNetUsers", "City");
            DropColumn("dbo.AspNetUsers", "PostalCode");
            DropColumn("dbo.VenueRegistrations", "DailyVisitorRegistration_Id");
            DropTable("dbo.DailyVisitorRegistrations");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.DailyVisitorRegistrations",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        RegistrationQuestionVersionId = c.Long(nullable: false),
                        UserId = c.String(maxLength: 128),
                        Answer1 = c.String(),
                        Answer2 = c.String(),
                        Answer3 = c.String(),
                        Answer4 = c.String(),
                        Answer5 = c.String(),
                        Answer6 = c.String(),
                        Answer7 = c.String(),
                        Answer8 = c.String(),
                        Answer9 = c.String(),
                        Answer10 = c.String(),
                        Answer11 = c.String(),
                        Answer12 = c.String(),
                        Answer13 = c.String(),
                        Answer14 = c.String(),
                        Answer15 = c.String(),
                        Answer16 = c.String(),
                        Answer17 = c.String(),
                        Answer18 = c.String(),
                        Answer19 = c.String(),
                        Answer20 = c.String(),
                        Created = c.DateTime(nullable: false),
                        AddressLine1 = c.String(),
                        AddressLine2 = c.String(),
                        City = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        PostalCode = c.String(),
                        Suburb = c.String(),
                        Updated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.VenueRegistrations", "DailyVisitorRegistration_Id", c => c.Long());
            AddColumn("dbo.AspNetUsers", "PostalCode", c => c.String());
            AddColumn("dbo.AspNetUsers", "City", c => c.String());
            AddColumn("dbo.AspNetUsers", "Suburb", c => c.String());
            AddColumn("dbo.AspNetUsers", "AddressLine2", c => c.String());
            AddColumn("dbo.AspNetUsers", "AddressLine1", c => c.String());
            AddColumn("dbo.AspNetUsers", "LastName", c => c.String());
            AddColumn("dbo.AspNetUsers", "FirstName", c => c.String());
            DropForeignKey("dbo.SubUsers", "MainUserId", "dbo.AspNetUsers");
            DropIndex("dbo.SubUsers", new[] { "MainUserId" });
            DropPrimaryKey("dbo.ApplicationUserOrganisations");
            DropTable("dbo.SubUsers");
            AddPrimaryKey("dbo.ApplicationUserOrganisations", new[] { "Organisation_Id", "ApplicationUser_Id" });
            CreateIndex("dbo.VenueRegistrations", "DailyVisitorRegistration_Id");
            CreateIndex("dbo.DailyVisitorRegistrations", "UserId");
            CreateIndex("dbo.DailyVisitorRegistrations", "RegistrationQuestionVersionId");
            AddForeignKey("dbo.DailyVisitorRegistrations", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.VenueRegistrations", "DailyVisitorRegistration_Id", "dbo.DailyVisitorRegistrations", "Id");
            AddForeignKey("dbo.DailyVisitorRegistrations", "RegistrationQuestionVersionId", "dbo.RegistrationQuestionVersions", "Id", cascadeDelete: true);
            RenameTable(name: "dbo.ApplicationUserOrganisations", newName: "OrganisationApplicationUsers");
        }
    }
}
