namespace CovidTracing.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PersonalInfoOnRegistration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DailyUserRegistrations", "AddressLine1", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "AddressLine2", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "City", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "FirstName", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "LastName", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "PostalCode", c => c.String());
            AddColumn("dbo.DailyUserRegistrations", "Suburb", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DailyUserRegistrations", "Suburb");
            DropColumn("dbo.DailyUserRegistrations", "PostalCode");
            DropColumn("dbo.DailyUserRegistrations", "LastName");
            DropColumn("dbo.DailyUserRegistrations", "FirstName");
            DropColumn("dbo.DailyUserRegistrations", "City");
            DropColumn("dbo.DailyUserRegistrations", "AddressLine2");
            DropColumn("dbo.DailyUserRegistrations", "AddressLine1");
        }
    }
}
