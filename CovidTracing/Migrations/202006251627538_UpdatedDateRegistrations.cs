namespace CovidTracing.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedDateRegistrations : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DailyUserRegistrations", "Updated", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DailyUserRegistrations", "Updated");
        }
    }
}
