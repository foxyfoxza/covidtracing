﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CovidTracing.Models;

namespace CovidTracing.ViewModels
{
    public class DailyRegistrationViewModel
    {
        public long Id { get; set; }
       
        public string Answer1 { get; set; }
        public string Answer2 { get; set; }
        public string Answer3 { get; set; }
        public string Answer4 { get; set; }
        public string Answer5 { get; set; }
        public string Answer6 { get; set; }
        public string Answer7 { get; set; }
        public string Answer8 { get; set; }
        public string Answer9 { get; set; }
        public string Answer10 { get; set; }
        public string Answer11 { get; set; }
        public string Answer12 { get; set; }
        public string Answer13 { get; set; }
        public string Answer14 { get; set; }
        public string Answer15 { get; set; }
        public string Answer16 { get; set; }
        public string Answer17 { get; set; }
        public string Answer18 { get; set; }
        public string Answer19 { get; set; }
        public string Answer20 { get; set; }
        
        public long SubUserId { get; set; }
        

        public DateTime Created { get; set; }
        public int VenueRegistrations { get; set; }
        public long? CurrentRegistrationQuestionVersionId { get; set; }
    }
}