﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CovidTracing.Infrastructure;
using CovidTracing.Models;
using QRCoder;

namespace CovidTracing.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context = ApplicationDbContext.Create();

        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}