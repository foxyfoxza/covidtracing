﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CovidTracing.Infrastructure;
using CovidTracing.Models;
using CovidTracing.ViewModels;
using Microsoft.AspNet.Identity;

namespace CovidTracing.Controllers
{
    public class RegistrationController : Controller
    {
        private readonly ApplicationDbContext _context = ApplicationDbContext.Create();

        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();
            var user = _context.Users.FirstOrDefault(o => o.Id == userId);
            var currentRegistrationQuestionVersionId = _context.CurrentRegistrationQuestionVersion?.Id ?? 0;
            var subUsers = _context.SubUsers.Where(o => o.MainUserId == userId).ToArray();
            var subUserIds = subUsers.Select(o => o.Id).ToArray();
            var dailyUserRegistrations = _context.DailyVisitorRegistrations
                .Where(o => subUserIds.Contains(o.SubUserId) &&
                            o.RegistrationQuestionVersionId == currentRegistrationQuestionVersionId)
                .OrderByDescending(o => o.Created).Take(10);
            var vm = new List<DailyRegistrationViewModel>();

            foreach (var dailyUserRegistration in dailyUserRegistrations)
            {
                var dailyRegistrationViewModel = new DailyRegistrationViewModel
                {
                    Id = dailyUserRegistration.Id,
                    Answer1 = dailyUserRegistration.Answer1,
                    Answer2 = dailyUserRegistration.Answer2,
                    Answer3 = dailyUserRegistration.Answer3,
                    Answer4 = dailyUserRegistration.Answer4,
                    Answer5 = dailyUserRegistration.Answer5,
                    Answer6 = dailyUserRegistration.Answer6,
                    Answer7 = dailyUserRegistration.Answer7,
                    Answer8 = dailyUserRegistration.Answer8,
                    Answer9 = dailyUserRegistration.Answer9,
                    Answer10 = dailyUserRegistration.Answer10,
                    Answer11 = dailyUserRegistration.Answer11,
                    Answer12 = dailyUserRegistration.Answer12,
                    Answer13 = dailyUserRegistration.Answer13,
                    Answer14 = dailyUserRegistration.Answer14,
                    Answer15 = dailyUserRegistration.Answer15,
                    Answer16 = dailyUserRegistration.Answer16,
                    Answer17 = dailyUserRegistration.Answer17,
                    Answer18 = dailyUserRegistration.Answer18,
                    Answer19 = dailyUserRegistration.Answer19,
                    Answer20 = dailyUserRegistration.Answer20,
                    
                    Created = dailyUserRegistration.Created,
                    VenueRegistrations = dailyUserRegistration.VenueRegistrations.Count
                };
                vm.Add(dailyRegistrationViewModel);
            }
            
            return View(vm);
        }

        private void CheckModel(DailyRegistrationViewModel model)
        {
            var userId = User.Identity.GetUserId();
            var subUser = _context.SubUsers.FirstOrDefault(o => o.MainUserId == userId && o.Id == model.SubUserId);
            if (subUser == null)
            {
                ModelState.AddModelError(nameof(model.SubUserId), "Please submit a value");
            }

            if (string.IsNullOrWhiteSpace(model.Answer1))
                ModelState.AddModelError(nameof(model.Answer1), "Please submit a value");

            if (string.IsNullOrWhiteSpace(model.Answer2))
                ModelState.AddModelError(nameof(model.Answer2), "Please submit a value");

            if (string.IsNullOrWhiteSpace(model.Answer3))
                ModelState.AddModelError(nameof(model.Answer3), "Please submit a value");

            if (string.IsNullOrWhiteSpace(model.Answer4))
                ModelState.AddModelError(nameof(model.Answer4), "Please submit a value");

            if (string.IsNullOrWhiteSpace(model.Answer5))
                ModelState.AddModelError(nameof(model.Answer5), "Please submit a value");

            if (string.IsNullOrWhiteSpace(model.Answer6))
                ModelState.AddModelError(nameof(model.Answer6), "Please submit a value");

            if (string.IsNullOrWhiteSpace(model.Answer7))
                ModelState.AddModelError(nameof(model.Answer7), "Please submit a value");

            if (string.IsNullOrWhiteSpace(model.Answer8))
                ModelState.AddModelError(nameof(model.Answer8), "Please submit a value");

            //if (string.IsNullOrWhiteSpace(model.FirstName))
            //    ModelState.AddModelError(nameof(model.FirstName), "Please submit a value");

            //if (string.IsNullOrWhiteSpace(model.LastName))
            //    ModelState.AddModelError(nameof(model.LastName), "Please submit a value");

            //if (string.IsNullOrWhiteSpace(model.AddressLine1))
            //    ModelState.AddModelError(nameof(model.AddressLine1), "Please submit a value");

            //if (string.IsNullOrWhiteSpace(model.City))
            //    ModelState.AddModelError(nameof(model.City), "Please submit a value");

            //if (string.IsNullOrWhiteSpace(model.Suburb))
            //    ModelState.AddModelError(nameof(model.Suburb), "Please submit a value");

            //if (string.IsNullOrWhiteSpace(model.PostalCode))
            //    ModelState.AddModelError(nameof(model.PostalCode), "Please submit a value");
        }

        [HttpGet]
        public ActionResult Create()
        {
            var userId = User.Identity.GetUserId();
            var user = _context.Users.FirstOrDefault(o => o.Id == userId);
            ViewBag.Mode = "Create";

            var subUsers = _context.SubUsers.Where(o => o.MainUserId == userId).ToArray();
            ViewBag.SubUserSelectList = new SelectList(subUsers.Select(o => new { Key = o.Id, Value = $"{o.FirstName}-{o.LastName}" }), "Key", "Value");
            
            var dailyRegistrationViewModel = new DailyRegistrationViewModel
            {
                CurrentRegistrationQuestionVersionId = _context.CurrentRegistrationQuestionVersion?.Id,
                Created = DateTime.Now
            };

            return View("Edit", dailyRegistrationViewModel);
        }


        [HttpPost]
        public ActionResult Create(DailyRegistrationViewModel model)
        {
            CheckModel(model);

            if (ModelState.IsValid)
            {
                 

                var dailyUserRegistration = new DailyVisitorRegistration
                {
                    Answer1 = model.Answer1,
                    Answer2 = model.Answer2,
                    Answer3 = model.Answer3,
                    Answer4 = model.Answer4,
                    Answer5 = model.Answer5,
                    Answer6 = model.Answer6,
                    Answer7 = model.Answer7,
                    Answer8 = model.Answer8,
                    Answer9 = model.Answer9,
                    Answer10 = model.Answer10,
                    Answer11 = model.Answer11,
                    Answer12 = model.Answer12,
                    Answer13 = model.Answer13,
                    Answer14 = model.Answer14,
                    Answer15 = model.Answer15,
                    Answer16 = model.Answer16,
                    Answer17 = model.Answer17,
                    Answer18 = model.Answer18,
                    Answer19 = model.Answer19,
                    Answer20 = model.Answer20,
                    SubUserId = model.SubUserId,
                    Created = DateTime.Now,
                    Updated = DateTime.Now,
                    RegistrationQuestionVersionId = _context.CurrentRegistrationQuestionVersion?.Id ?? 0
                };

                _context.DailyVisitorRegistrations.Add(dailyUserRegistration);

                _context.SaveChanges();
                return RedirectToAction("Index");
            }

            return View("Edit", model);
        }


        [HttpGet]
        public ActionResult Edit(long id)
        {
            var userId = User.Identity.GetUserId();
            
            ViewBag.Mode = "Edit";
            var user = _context.Users.FirstOrDefault(o => o.Id == userId);
            var subUsers = _context.SubUsers.Where(o => o.MainUserId == userId).ToArray();
            var subUserIds = subUsers.Select(o => o.Id).ToArray();

            var dailyUserRegistration =
                _context.DailyVisitorRegistrations.FirstOrDefault(o => subUserIds.Contains(o.SubUserId) && o.Id == id);
            if (dailyUserRegistration == null ||
                dailyUserRegistration.RegistrationQuestionVersionId != _context.CurrentRegistrationQuestionVersion.Id ||
                dailyUserRegistration.VenueRegistrations.Any())
            {
                ModelState.AddModelError("errors", "Could not edit the record at this time");
                return RedirectToAction("Index");
            }
            ViewBag.SubUserSelectList = new SelectList(subUsers.Select(o => new { Key = o.Id, Value = $"{o.FirstName}-{o.LastName}" }), "Key", "Value");

            var dailyRegistrationViewModel = new DailyRegistrationViewModel
            {
                Answer1 = dailyUserRegistration.Answer1,
                Answer2 = dailyUserRegistration.Answer2,
                Answer3 = dailyUserRegistration.Answer3,
                Answer4 = dailyUserRegistration.Answer4,
                Answer5 = dailyUserRegistration.Answer5,
                Answer6 = dailyUserRegistration.Answer6,
                Answer7 = dailyUserRegistration.Answer7,
                Answer8 = dailyUserRegistration.Answer8,
                Answer9 = dailyUserRegistration.Answer9,
                Answer10 = dailyUserRegistration.Answer10,
                Answer11 = dailyUserRegistration.Answer11,
                Answer12 = dailyUserRegistration.Answer12,
                Answer13 = dailyUserRegistration.Answer13,
                Answer14 = dailyUserRegistration.Answer14,
                Answer15 = dailyUserRegistration.Answer15,
                Answer16 = dailyUserRegistration.Answer16,
                Answer17 = dailyUserRegistration.Answer17,
                Answer18 = dailyUserRegistration.Answer18,
                Answer19 = dailyUserRegistration.Answer19,
                Answer20 = dailyUserRegistration.Answer20,
                Created = DateTime.Now
            };
            
            return View("Edit", dailyRegistrationViewModel);
        }


        [HttpPost]
        public ActionResult Edit(long id, DailyRegistrationViewModel model)
        {
            CheckModel(model);

            var userId = User.Identity.GetUserId();
            var user = _context.Users.FirstOrDefault(o => o.Id == userId);
            var subUsers = _context.SubUsers.Where(o => o.MainUserId == userId).ToArray();
            var subUserIds = subUsers.Select(o => o.Id).ToArray();

            if (!subUserIds.Contains(model.SubUserId))
            {
                ModelState.AddModelError(nameof(model.SubUserId), "Please submit a value");
            }

            var dailyUserRegistration =
                _context.DailyVisitorRegistrations.FirstOrDefault(o => subUserIds.Contains(o.SubUserId) && o.Id == id);
            if (dailyUserRegistration == null ||
                dailyUserRegistration.RegistrationQuestionVersionId != _context.CurrentRegistrationQuestionVersion.Id ||
                dailyUserRegistration.VenueRegistrations.Any())
            {
                ModelState.AddModelError("errors", "Could not edit the record at this time");
                return RedirectToAction("Index");
            }

            if (ModelState.IsValid)
            {
                dailyUserRegistration.Answer1 = model.Answer1;
                dailyUserRegistration.Answer2 = model.Answer2;
                dailyUserRegistration.Answer3 = model.Answer3;
                dailyUserRegistration.Answer4 = model.Answer4;
                dailyUserRegistration.Answer5 = model.Answer5;
                dailyUserRegistration.Answer6 = model.Answer6;
                dailyUserRegistration.Answer7 = model.Answer7;
                dailyUserRegistration.Answer8 = model.Answer8;
                dailyUserRegistration.Answer9 = model.Answer9;
                dailyUserRegistration.Answer10 = model.Answer10;
                dailyUserRegistration.Answer11 = model.Answer11;
                dailyUserRegistration.Answer12 = model.Answer12;
                dailyUserRegistration.Answer13 = model.Answer13;
                dailyUserRegistration.Answer14 = model.Answer14;
                dailyUserRegistration.Answer15 = model.Answer15;
                dailyUserRegistration.Answer16 = model.Answer16;
                dailyUserRegistration.Answer17 = model.Answer17;
                dailyUserRegistration.Answer18 = model.Answer18;
                dailyUserRegistration.Answer19 = model.Answer19;
                dailyUserRegistration.Answer20 = model.Answer20;
                dailyUserRegistration.SubUserId = model.SubUserId;
                dailyUserRegistration.Updated = DateTime.Now;

                _context.SaveChanges();
                return RedirectToAction("Index");
            }

            return View("Edit", model);
        }


        [HttpGet]
        public ActionResult Delete(long id)
        {
            var userId = User.Identity.GetUserId();
            ViewBag.Mode = "Delete";
            var subUsers = _context.SubUsers.Where(o => o.MainUserId == userId).ToArray();
            var subUserIds = subUsers.Select(o => o.Id).ToArray();
            var dailyUserRegistration =
                _context.DailyVisitorRegistrations.FirstOrDefault(o => subUserIds.Contains(o.SubUserId) && o.Id == id);
            if (dailyUserRegistration == null ||
                dailyUserRegistration.RegistrationQuestionVersionId != _context.CurrentRegistrationQuestionVersion.Id ||
                dailyUserRegistration.VenueRegistrations.Any())
            {
                ModelState.AddModelError("errors", "Could not edit the record at this time");
                return RedirectToAction("Index");
            }

            ViewBag.SubUserSelectList = new SelectList(subUsers.Select(o => new { Key = o.Id, Value = $"{o.FirstName}-{o.LastName}" }), "Key", "Value");

            var dailyRegistrationViewModel = new DailyRegistrationViewModel
            {
               
                Answer1 = dailyUserRegistration.Answer1,
                Answer2 = dailyUserRegistration.Answer2,
                Answer3 = dailyUserRegistration.Answer3,
                Answer4 = dailyUserRegistration.Answer4,
                Answer5 = dailyUserRegistration.Answer5,
                Answer6 = dailyUserRegistration.Answer6,
                Answer7 = dailyUserRegistration.Answer7,
                Answer8 = dailyUserRegistration.Answer8,
                Answer9 = dailyUserRegistration.Answer9,
                Answer10 = dailyUserRegistration.Answer10,
                Answer11 = dailyUserRegistration.Answer11,
                Answer12 = dailyUserRegistration.Answer12,
                Answer13 = dailyUserRegistration.Answer13,
                Answer14 = dailyUserRegistration.Answer14,
                Answer15 = dailyUserRegistration.Answer15,
                Answer16 = dailyUserRegistration.Answer16,
                Answer17 = dailyUserRegistration.Answer17,
                Answer18 = dailyUserRegistration.Answer18,
                Answer19 = dailyUserRegistration.Answer19,
                Answer20 = dailyUserRegistration.Answer20,
                Created = DateTime.Now
            };
            
            return View("Edit", dailyRegistrationViewModel);
        }


        [HttpPost]
        public ActionResult Delete(long id, DailyRegistrationViewModel model)
        {
            var userId = User.Identity.GetUserId();
            var subUserIds = _context.SubUsers.Where(o => o.MainUserId == userId).Select(o => o.Id).ToArray();

            var dailyUserRegistration = 
                _context.DailyVisitorRegistrations.FirstOrDefault(o => subUserIds.Contains(o.SubUserId) && o.Id == id);

            if (dailyUserRegistration == null ||
                dailyUserRegistration.RegistrationQuestionVersionId != _context.CurrentRegistrationQuestionVersion.Id ||
                dailyUserRegistration.VenueRegistrations.Any())
            {
                ModelState.AddModelError("errors", "Could not delete the record at this time");
                return RedirectToAction("Index");
            }

            _context.DailyVisitorRegistrations.Remove(dailyUserRegistration);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}