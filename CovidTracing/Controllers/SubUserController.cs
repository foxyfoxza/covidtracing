﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Results;
using System.Web.Mvc;
using CovidTracing.Infrastructure;
using CovidTracing.Models;
using CovidTracing.ViewModels;
using Microsoft.AspNet.Identity;

namespace CovidTracing.Controllers
{
    public class SubUserController : Controller
    {
        private readonly ApplicationDbContext _context = ApplicationDbContext.Create();

        // GET: SubUser
        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();
            var subUsers = _context.SubUsers.Where(o=> o.MainUserId == userId).ToArray();

           return View(subUsers);
        }

        // GET: SubUser/Details/5
        public ActionResult Details(int id)
        {
            var framePath = Server.MapPath("~/Content/Images/myframe.png");
            var imagePath = Server.MapPath("~/Content/Images/myimage.png");
            
            var userId = User.Identity.GetUserId();
            var subUser = _context.SubUsers.FirstOrDefault(o => o.MainUserId == userId && o.Id == id);

            ViewBag.QrCode = QrCodeHelper.GetQrCode(subUser, framePath, imagePath);
            return View(subUser);
        }

        // GET: SubUser/Create
        public ActionResult Create()
        {
            return View(new SubUser());
        }

        // POST: SubUser/Create
        [HttpPost]
        public ActionResult Create(SubUser subUser)
        {
            try
            {
                subUser.MainUserId = User.Identity.GetUserId();
                subUser.UserCode = _context.GetUserCode();
                _context.SubUsers.Add(subUser);
                _context.SaveChanges();

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                return View(subUser);
            }
        }

        // GET: SubUser/Edit/5
        public ActionResult Edit(int id)
        {
            var userId = User.Identity.GetUserId();
            var subUser = _context.SubUsers.FirstOrDefault(o => o.MainUserId == userId && o.Id == id);

            return View(subUser);
        }

        // POST: SubUser/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, SubUser subUser)
        {
            try
            {
                var db = _context.SubUsers.FirstOrDefault(o => o.Id == id);
                db.AddressLine2 = subUser.AddressLine2;
                db.AddressLine1 = subUser.AddressLine1;
                db.City= subUser.City;
                db.FirstName = subUser.FirstName;
                db.LastName = subUser.LastName;
                db.PostalCode = subUser.PostalCode;
                db.Suburb = subUser.Suburb;
                db.UserCode = subUser.UserCode;

                _context.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View(subUser);
            }
        }

        // GET: SubUser/Delete/5
        public ActionResult Delete(int id)
        {
            var userId = User.Identity.GetUserId();
            var subUser = _context.SubUsers.FirstOrDefault(o => o.MainUserId == userId && o.Id == id);

            return View(subUser);
        }

        // POST: SubUser/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, SubUser subUser)
        {
            try
            {
                var userId = User.Identity.GetUserId();
                var db = _context.SubUsers.FirstOrDefault(o => o.MainUserId == userId && o.Id == id);

                _context.SubUsers.Remove(db);
                _context.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View(subUser);
            }
        }
    }
}
