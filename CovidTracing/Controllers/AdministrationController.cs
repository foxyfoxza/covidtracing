﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CovidTracing.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace CovidTracing.Controllers
{
    public class AdministrationController : Controller
    {
        private readonly ApplicationDbContext _context = ApplicationDbContext.Create();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        
        public AdministrationController()
        {
            
        }

        public AdministrationController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Users()
        {
            var userId = User.Identity.GetUserId();
            var loggedInUser = _context.Users.Include(o=> o.Organisations).FirstOrDefault(o => o.Id == userId);

            var organizations = loggedInUser.Organisations.ToList();
            var users = new List<ApplicationUser>();
            foreach (var org in organizations)
            {
                users.AddRange(org.Users);
            }

            return View(users.Distinct());
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

    }
}