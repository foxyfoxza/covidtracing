﻿using System;
using System.Linq;
using CovidTracing;
using CovidTracing.Models;
using CovidTracing.Resources;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Startup))]

namespace CovidTracing
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            try
            {
                //check questions
                var context = ApplicationDbContext.Create();
                context.CheckRegistrationQuestionVersions();
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}